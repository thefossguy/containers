#!/usr/bin/env bash

set -x

#podman exec -u www-data nextcloud-govinda /usr/local/bin/php -f /var/www/html/occ maintenance:mode --on
podman exec -u www-data nextcloud-govinda /usr/local/bin/php -f /var/www/html/occ app:update --all
podman exec -u www-data nextcloud-govinda /usr/local/bin/php -f /var/www/html/occ maintenance:repair
podman exec -u www-data nextcloud-govinda /usr/local/bin/php -f /var/www/html/occ upgrade
podman exec -u www-data nextcloud-govinda /usr/local/bin/php -f /var/www/html/occ maintenance:mode --off
